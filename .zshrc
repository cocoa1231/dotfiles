# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
  export ZSH=/home/cocoa/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
# ZSH_THEME="bira"
# ZSH_THEME="gnzh"
# ZSH_THEME="robbyrussell"
ZSH_THEME="agnoster"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git battery)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Created by newuser for 5.3.1
# alias firefox="env GTK_THEME=Breeze:light firefox %u"
#
# AUR Aliases
alias aur="pacaur -S --noconfirm"
alias aurs="pacaur -Ss --noconfirm"
alias aurup="pacaur -Syu --aur"  
# Pacman Aliases
alias pS="pacaur -S"
alias pR=" pacaur -R"
alias pRs="pacaur -Rs"
alias pSy="pacaur -Sy"
alias pRsc="pacaur -Rsc"
alias pSs="pacaur -Ss"
alias pSsq="pacaur -Ssq"
alias pSyu="pacaur -Syu"
alias pSyy="pacaur -Syy"
alias pSyyu="pacaur -Syyu"
alias p="pacaur"
alias optimize="sudo pacman-optimize"
# Other Aliases
alias xclip="xclip -selection clipboard"
alias PT="./home/cocoa1231/Quick_Dump/PT/Popcorn-Time"
alias readme="touch README.md; typora README.md"
alias editconfig="vim ~/.config/i3/config"
alias runapp="bash /home/cocoa/.runapp.sh"
alias appRoot="cd /home/cocoa/CexpSampler"
alias listvm="ls /home/cocoa/VirtualBox\ VMs/"

# SSH
#eval `keychain --agents ssh --eval ~/.ssh/id_rsa`
#clear

unset ANDROID_HOME
unset ANDROID_SDK_ROOT
echo $PS1, $RANDOM_THEME >> ~/themes.txtwq
export PATH=$PATH:/home/cocoa/Downloads/SDK/tools
export BROWSER=firefox
export VM="Windows 7"
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
eval `keychain --quiet --agents ssh --eval ~/.ssh/id_rsa`;

clear
neofetch
