set nocompatible              " be iMproved, required
filetype off 				  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" My plugins!
Plugin 'Valloric/YouCompleteMe'
Plugin 'lervag/vimtex'
Plugin 'vim-syntastic/syntastic'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'
Plugin 'jiangmiao/auto-pairs'
Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/nerdtree'
Plugin 'reedes/vim-pencil' " Super-powered writing things
Plugin 'tpope/vim-abolish' " Fancy abbreviation replacements
Plugin 'junegunn/limelight.vim' " Highlights only active paragraph
Plugin 'junegunn/goyo.vim' " Full screen writing mode
Plugin 'reedes/vim-lexical' " Better spellcheck mappings
Plugin 'reedes/vim-litecorrect' " Better autocorrections
Plugin 'reedes/vim-textobj-sentence' " Treat sentences as text objects
Plugin 'reedes/vim-wordy' " Weasel words and passive voice
Plugin 'Lokaltog/vim-powerline'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

let g:ycm_path_to_python_interpreter='/usr/bin/python'
let g:ycm_server_python_interpreter='/usr/bin/python'

let g:ycm_python_binary_path='/usr/bin/python'

let g:ycm_server_keep_logfiles = 1
let g:ycm_server_log_level = 'debug'
let g:ycm_semantic_triggers = {
	\   'python': [ 're!\w{2}' ],
    \   'c' : ['->', '.'],
    \   'objc' : ['->', '.', 're!\[[_a-zA-Z]+\w*\s', 're!^\s*[^\W\d]\w*\s',
    \             're!\[.*\]\s'],
    \   'ocaml' : ['.', '#'],
    \   'cpp,objcpp' : ['->', '.', '::'],
    \   'perl' : ['->'],
    \   'php' : ['->', '::'],
    \   'cs,java,javascript,typescript,d,python,perl6,scala,vb,elixir,go' : ['.'],
    \   'ruby' : ['.', '::'],
    \   'lua' : ['.', ':'],
    \   'erlang' : [':'],
	\ }
let g:ycm_autoclose_preview_window_after_completion=1
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"


" Shit I actually typed and not just copy-pasted
set ai nu ts=4 sw=4 expandtab mouse=a clipboard+=unnamedplus incsearch
syntax on

map <F2> <esc>:lclose<return>i<right>
map <F3> <esc>:Errors<return>i<right>
imap <F2> <esc>:lclose<return>i<right>
imap <F3> <esc>:Errors<return>i<right>


 "copy and paste
vmap <C-c> :w !xclip -i -sel c<return><return>

" Autowrite boilerplate code
au BufNewFile *.py 0r ~/.vim/py.skel | let IndentStyle = "python"

" YouCompleteMe stuff
let g:ycm_min_num_of_chars_for_completion = 2
let g:ycm_auto_trigger = 1

let g:python_host_prog='/usr/bin/python2'
let g:python3_host_prog='/usr/bin/python3'


